import Observable.IphoneObservableImpl;
import Observable.SamsungObservableImpl;
import Observable.StockObservable;
import Observer.EmailAlertObserverImpl;
import Observer.MobileAlertObserverImpl;
import Observer.NotificationAlertObserver;

public class Store {
    public static void main(String[] args) {
        StockObservable iphoneStockObservable = new IphoneObservableImpl();
        StockObservable samsungStockObservable = new SamsungObservableImpl();

        NotificationAlertObserver observer1 = new EmailAlertObserverImpl("Samsung@gmail.com", iphoneStockObservable);
//        NotificationAlertObserver observer2 = new EmailAlertObserverImpl("shreya@gmail.com", iphoneStockObservable);
//        NotificationAlertObserver observer3 = new MobileAlertObserverImpl("Shreyash", iphoneStockObservable);

        iphoneStockObservable.add(observer1);
//        iphoneStockObservable.add(observer2);
//        iphoneStockObservable.add(observer3);

        iphoneStockObservable.setStockCount(10);


//        NotificationAlertObserver observer2 = new EmailAlertObserverImpl("Samsung@gmail.com", iphoneStockObservable);
//        samsungStockObservable.add(observer2);
//        samsungStockObservable.add(observer2);
//        samsungStockObservable.add(observer3);

//        samsungStockObservable.setStockCount(10);
    }
}
